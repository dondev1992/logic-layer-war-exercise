import java.util.ArrayList;
import java.util.*;

public class Player {

    private ArrayList<Card> hand;

    public Player(ArrayList<Card> hand)
    {
        this.hand = hand;
    }

    public Player()
    {
        hand = new ArrayList<Card>();
    }

    public ArrayList<Card> getHand() {
        return hand;
    }
}