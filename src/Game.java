import java.util.ArrayList;
import java.util.*;

public class Game {

    public static void createDeck(String[] cardRanks, String[] cardSuits) {
        // create deck arraylist
        ArrayList<Card> cardDeck = new ArrayList<>();
        // loop through suit array
        for (int i = 0; i < 4; i++) {
            //assign suit to a varible
            String suit = cardSuits[i];
            //loop through ranks
            for (int j = 0; j < 13; j++) {
                // create card and assign rank and suit variable
                String rank = cardRanks[j];
                Card aCard = new Card(rank, suit);
                // add card to deck arraylist
                cardDeck.add(aCard);
            }
        }
        for (Card card: cardDeck) {
            card.displayCard();
        }
    }

    public static void shuffleDeck(ArrayList<Card> deck) {
        Collections.shuffle(deck);
        for (Card card: deck) {
            card.displayCard();
        }
    }

    public static void dealDeck(Deck deck, Player player1, Player player2) {
        for(int i = 0; i < deck.getCardDeck().size(); i++){
            Card currentCard = deck.getCardDeck().get(i);
            if(i % 2 != 0) {
                player1.getHand().add(currentCard);
            }
            else {
                player2.getHand().add(currentCard);
            }
        }
    }

    public static void main(String[] args) {

        final String[] cardSuits = {
                "SPADES", "DIAMONDS", "CLUBS", "HEARTS"
        };

        final String[] cardRanks = {
                "Ace", "2", "3","4","5","6","7","8","9","10","Jack","Queen","King",
        };

        ArrayList<Card> cardDeck = new ArrayList<>();
        Player player1 = new Player();
        Player player2 = new Player();
        Deck deck = new Deck(cardRanks, cardSuits, cardDeck);

        // Shuffle cards
        shuffleDeck(cardDeck);
        System.out.println(cardDeck.size());

        // Divide deck into  hands
        dealDeck(deck, player1, player2);
        System.out.println(player1.getHand());
        System.out.println(player2.getHand());
        // display card off top of both hand arrays
    }



}