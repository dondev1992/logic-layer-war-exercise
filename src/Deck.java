import java.util.ArrayList;

public class Deck {
    private ArrayList<Card> cardDeck;

    private final String[] cardSuits;
    private final String[] cardRanks;

    public Deck(String[] cardRanks, String[] cardSuits, ArrayList<Card> cardDeck) {
        this.cardRanks = cardRanks;
        this.cardSuits = cardSuits;
        this.cardDeck = cardDeck;

        // loop through suit array
        for (int i = 0; i < 4; i++) {
            //assign suit to a varible
            String suit = cardSuits[i];
            //loop through ranks
            for (int j = 0; j < 13; j++) {
                // create card and assign rank and suit variable
                String rank = cardRanks[j];
                Card aCard = new Card(rank, suit);
                // add card to deck arraylist
                this.cardDeck.add(aCard);
            }
        }
    }

    public ArrayList<Card> getCardDeck() {
        return cardDeck;
    }
}
