public class Card {
    private String cardRank;
    private String cardSuit;

    public Card(String cardRank, String cardSuit) {
        this.cardRank = cardRank;
        this.cardSuit = cardSuit;
    }

    public String getCardRank() {
        return cardRank;
    }

    public String getCardSuit() {
        return cardSuit;
    }

    public void displayCard() {
        System.out.println("Card: " + cardRank + " of " + cardSuit);
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardRank='" + cardRank + '\'' +
                ", cardSuit='" + cardSuit + '\'' +
                '}';
    }
}
